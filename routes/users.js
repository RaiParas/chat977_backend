var express = require('express');
var router = express.Router();
const bcrypt = require('bcrypt');
var con = require("../database/connect");

var isBase64 = require('is-base64');


var base64Img = require('base64-img');

var jwt = require('jsonwebtoken');





const { image } = require('../utils/saveimage');

/* GET users listing. */
router.get('/user', function (req, res, next) {
  res.send('respond with a resource');
});

/* GET users listing. */
router.post('/register-user', function (req, res, next) {

  var { full_name, user_name, date_of_birth, email, phone, gender, address, city, country, password, image } = req.body;


  console.log(image);
  // Save image 
  var filename = (new Date()).getTime();
  var filepath = base64Img.imgSync(image, 'resources', filename, function (err, filepath) {
    if (err) {
      return res.status(400).json({
        message: "Invalid profile picture",
        status: 400,
      });
    }
    return filepath;
  });

  var level = 0;
  var join_date = new Date();
  join_date = join_date.getFullYear() + "-" + (join_date.getMonth() + 1) + "-" + join_date.getDate();
  bcrypt.genSalt(10, function (err, salt) {
    if (err)
      return res.status(err.status).send({
        message: err.message,
        status: err.status,
      });
    bcrypt.hash(password, salt, function (err, pass) {
      console.log(pass);

      var sql = `INSERT INTO users (
      full_name, user_name, password,
      email, date_of_birth, photo_profile,
      phone, level, join_date, gender, 
      address, city, country) VALUES 
      ( '${full_name}', '${user_name}', '${pass}', 
        '${email}', '${date_of_birth}', '${filepath}', '${phone}', '${level}', 
        '${join_date}', '${gender}', '${address}', 
        '${city}', '${country}')`;


      con.query(sql, function (error, results) {

        if (error) {
          console.log(error);
          return res.status(error.code).json({
            message: error.message,
            status: error.code,
          });
        }
        console.log(results.insertId);
        // image.saveImage(image).then((filename) => {
        //   console.log(filename)
        //   var insertPicture = `INSERT INTO profile_picture ( image_name,  user_id) VALUES ( '${filename}', '${results.insertId}')`;
        //   con.query(insertPicture, function (error, results) {
        //     if (error)
        //       return res.status(err.status).send({
        //         message: err.message,
        //         status: err.status,
        //       });
        //     return res.status(results.status).send({
        //       message: results.message,
        //       status: results.status,
        //     });
        //   });
        // });

        res.status(200).json({
          message: "Data saved",
          status: 200,
        })


      });
    });
  });



});

router.post('/login', function (req, res, next) {
  var { email_or_username, password } = req.body;
  var sql = `SELECT id, password FROM users WHERE user_name='${email_or_username}' OR email = '${email_or_username}'`;
  con.query(sql, function (error, results) {
    if (error) {
      return res.status(400).json({
        message: "Error occured", status: 400,
      });
    }
    var token = jwt.sign({ email_or_username: email_or_username, id: results[0].id }, "chat977-paras");
    if (results.length == 0) {
      return res.status(206).json({
        message: "Username or Email not found", status: 400,
      });
    }
    bcrypt.compare(password, results[0].password).then((result) => {
      if (result)
        return res.send({
          token: token,
          message: 'User login successful', status: 200,
        });
      else
        return res.send({
          message: 'Password doesnot match', status: 400,
        });
    });
  });
});



router.post('/getUserInfo', function (req, res, next) {
  var token = req.header('authorization');
  console.log(token);

  jwt.verify(token, "chat977-paras", function (err, decoded) {
    if (err) {
      return res.status(400).json({
        message: "Invalid Token",
        status: 400,
      });
    }
    var sql = `SELECT full_name, user_name, email, date_of_birth, phone, photo_profile, level, join_date, gender, address, city, country FROM users WHERE id=${decoded.id}`;
    con.query(sql, function (error, results) {
      if (error) {
        return res.status(400).json({
          message: "Error occured", status: 400,
        });
      }
      if (results.length == 0) {
        return res.status(206).json({
          message: "No data found", status: 400,
        });
      }
      return res.send({
        token: token,
        message: 'User Information',
        status: 200,
        data: {
          full_name: results[0].full_name,
          user_name: results[0].user_name,
          email: results[0].email,
          date_of_birth: results[0].date_of_birth,
          phone: results[0].phone,
          photo: results[0].photo_profile,
          level: results[0].level,
          join_date: results[0].join_date,
          gender: results[0].gender,
          city: results[0].city,
          country: results[0].country,
          address: results[0].address
        }
      });
    });
  });

});

router.post('/verifyToken', function (req, res, next) {
  var token = req.header('authorization');
  console.log(token);
  jwt.verify(token, "chat977-paras", function (err, decoded) {
    if (err) {
      return res.status(400).json({
        message: "Invalid Token",
        status: 400,
      });
    }
    return res.status(200).json({
      message: "Token Verified", status: 200,
    });
  });

});
module.exports = router;
