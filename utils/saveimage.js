
var base64Img = require('base64-img');
const imageThumbnail = require('image-thumbnail');

const saveImage = async (image) => {
    var filename = (new Date()).getTime();
    var filepath = await base64Img.imgSync(image, 'resources', filename);
    var ext = filepath.split('.').pop();
    try {
        imageThumbnail(filepath, { width: 200, height: 200, responseType: 'base64' }).then((thumbnail) => {
            base64Img.imgSync("data:image/" + ext + ";base64," + thumbnail, 'resources', filename + "_thumb_200X200");
        });
        imageThumbnail(filepath, { width: 700, height: 700, responseType: 'base64' }).then((thumbnail) => {
            base64Img.imgSync("data:image/" + ext + ";base64," + thumbnail, 'resources', filename + "_thumb_700X700");
        });
    } catch (err) {
        console.error(err);
    }
    return filepath;
}

exports.saveImage = saveImage;